﻿using PaintDotNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace BatchProcessAutomater
{
    /// <summary>
    /// Made my own, so the JPEG quality settings won't show - we will save everything at "100" quality
    /// </summary>
    class NoDialogJPEG : FileType
    {
        public NoDialogJPEG() : base("JPEG", FileTypeFlags.SupportsSaving, new string [] {".jpg", ".jpeg", ".jpe", ".jfif"} )
        {
        }

        public override Document OnLoad(System.IO.Stream input)
        {
            throw new NotImplementedException();
        }

        public override void OnSave(Document input, System.IO.Stream output, SaveConfigToken token, Surface scratchSurface, ProgressEventHandler callback)
        {
            ImageCodecInfo icf = GdiPlusFileType.GetImageCodecInfo(ImageFormat.Jpeg);
            EncoderParameters parms = new EncoderParameters(1);
            EncoderParameter parm = new EncoderParameter(Encoder.Quality, (long)100);
            parms.Param[0] = parm;
            scratchSurface.Clear(ColorBgra.White);
            using (RenderArgs ra = new RenderArgs(scratchSurface))
            {
                input.Render(ra, false);
            }
            using (Bitmap bitmap = scratchSurface.CreateAliasedBitmap())
            {
                GdiPlusFileType.LoadProperties(bitmap, input);
                bitmap.Save(output, icf, parms);
            }
        }
    }
}
