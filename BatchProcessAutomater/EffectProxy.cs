﻿using PaintDotNet.Effects;
using pyrochild.effects.scriptlab;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BatchProcessAutomater
{
    /// <summary>
    /// Stops underlying CreateDialogConfig from showing
    /// </summary>
    class ScriptEffectProxy : Effect
    {
        Effect effect;
        public ScriptEffectProxy() : base("Effect Proxy", ScriptLab.StaticIcon, ScriptLab.StaticSubMenuName, EffectFlags.None)
        {
            this.effect = new ScriptLab();
        }

        public static EffectConfigToken OverrideEffectToken = null;

        public override void Render(EffectConfigToken parameters, PaintDotNet.RenderArgs dstArgs, PaintDotNet.RenderArgs srcArgs, System.Drawing.Rectangle[] rois, int startIndex, int length)
        {
            if (OverrideEffectToken != null)
                parameters = OverrideEffectToken;
            effect.Render(parameters, dstArgs, srcArgs, rois, startIndex, length);
        }
    }
}
