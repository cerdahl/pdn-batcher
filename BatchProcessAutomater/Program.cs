﻿using PaintDotNet;
using PaintDotNet.Effects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Forms;

namespace BatchProcessAutomater
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        [STAThread]
        static void Main()
        {
            DllTweaker.ConvertPaintNetAssemblies();

            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            Control.CheckForIllegalCrossThreadCalls = true;
            Application.SetCompatibleTextRenderingDefault(false);
            Application.EnableVisualStyles();
            Application.Run(new BatchConfig());
        }

        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.StartsWith("PdnLib"))
                return typeof(ColorBgra).Assembly;
            else if (args.Name.StartsWith("ScriptLab"))
            {
                string newPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Effects");
                newPath = Path.Combine(newPath, "ScriptLab.dll");
                return Assembly.LoadFrom(newPath);
            }
            return null;
        }
    }
}

