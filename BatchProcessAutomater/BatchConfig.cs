﻿using PaintDotNet;
using PaintDotNet.Effects;
using pyrochild.effects.scriptlab;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BatchProcessAutomater
{
    public partial class BatchConfig : Form
    {
        public BatchConfig()
        {
            InitializeComponent();
            string PicturesFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            txtSourceFolder.Text = PicturesFolder;
            txtDestFolder.Text = Path.Combine(PicturesFolder, "BatchProcessOutput");
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(txtDestFolder.Text)) Directory.CreateDirectory(txtDestFolder.Text);
            Automation.Bridge(txtSourceFolder.Text, txtDestFolder.Text, txtFilter.Text, txtScriptPath.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtSourceFolder.Text = folderBrowserDialog1.SelectedPath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtDestFolder.Text = folderBrowserDialog1.SelectedPath;
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtScriptPath.Text = openFileDialog1.FileName;
        }
    }
}
